Gulliv'Air
===

## Présentation du projet

Gulliv'Air est un projet générique de capteurs du type projet [Ambassad'Air](https://gitlab.com/AmbassadAir).

Ambassad'Air est un projet qui vise à mobiliser les habitants sur la qualité de l'air à Rennes.

Le projet se décompose en plusieurs réalisations de boîtiers de mesures.  
Chaque boîtier a ses propres caractéristiques techniques, mais tous ont vocation à être reproduit librement.

## Documentation du projet

Vous pouvez trouver la documentation du projet Gulliv'Air dans le [Wiki](https://gitlab.com/GullivAir/Doc/wikis/home).

### Contributions

Gulliv'Air est un projet libre et nous sommes très heureux d'accepter les contributions de la communauté.  
Veuillez vous référer à la page [Contributions](https://gitlab.com/GullivAir/Doc/wikis/contributions) pour plus de détails.

## Licence

Le projet est sous licence GPLv3. Pour plus d'informations, regarder les droits en visitant cette page [https://www.gnu.org/licenses/gpl-3.0.html](https://www.gnu.org/licenses/gpl-3.0.html).

## Structure du projet

 Gulliv'Air est composé de 4 grandes parties :
 - [Collect](https://gitlab.com/GullivAir/Collect) pour la récupération des données depuis les capteurs et restitution au détenteur du boitier.
 - [Transmission](https://gitlab.com/GullivAir/) pour la gestion du transfert des données depuis les boitiers jusqu'au serveur.
 - [Server](https://gitlab.com/GullivAir/Server) pour la gestion des données envoyées par les capteurs et les requètes de visualisation.
 - [Visualization](https://gitlab.com/GullivAir/Visualization) pour la mise en valeur et l'exploitation des données collectées.

 à celà s'ajoute 2 projets annexes:
 - [sand-box](https://gitlab.com/GullivAir/sand-box) pour tous les tests de techno entre autres.
 - [Doc](https://gitlab.com/GullivAir/Doc) (ce projet) pour gérer la documentation commune du projet Gulliv'Air.
